﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * QuitManager
 * Created by: Tim Falken
 **/

public class QuitManager : MonoBehaviour 
{
	public void Quit()
	{
		Application.Quit ();
	}
}
