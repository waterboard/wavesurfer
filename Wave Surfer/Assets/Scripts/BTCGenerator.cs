﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * BTCGenerator
 * Created by: Tim Falken
 **/

public class BTCGenerator
{
	public double BTCPerTick;
	public float PayoutInterval;

	public int Amount = 0;

	private float currentTimer = 0;

	public float TimerProgress
	{
		get
		{
			return Mathf.Clamp (1 - (currentTimer / ((PayoutInterval) / Amount)), 0, 1);
		}
	}

	public float TrueIntervalInSeconds
	{
		get
		{
			return (PayoutInterval) / Amount;
		}
	}

	public double BTCPerSecond
	{
		get
		{
			if (Amount == 0)
			{
				return 0;
			}

			return (BTCPerTick * Amount);// / PayoutInterval;
		}
	}

	public double TrueOutputPerTick
	{
		get
		{
			return BTCPerTick * PayoutInterval;
		}
	}

	public BTCGenerator(double btcPerTick, float payoutInterval)
	{
		BTCPerTick = btcPerTick;
		PayoutInterval = payoutInterval;
		currentTimer = (PayoutInterval) / Amount;
	}

	public void Update()
	{
		if (currentTimer > (PayoutInterval) / Amount)
		{
			currentTimer = (PayoutInterval) / Amount;
		}

		currentTimer -= Time.deltaTime;

		if (currentTimer <= 0)
		{
			if (Amount == 0)
			{
				currentTimer = 0.1f;
				return;
			}

			currentTimer = (PayoutInterval) / Amount;

			double frameSkipMultiplier = (double)Math.Max(1.0f, Time.deltaTime / currentTimer);

			CurrencyManager.AddBTC (TrueOutputPerTick * frameSkipMultiplier);
		}
	}
}
