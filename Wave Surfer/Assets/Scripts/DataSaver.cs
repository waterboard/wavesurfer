﻿using System;
using UnityEngine;
using System.Collections;

/**
 * DataSaver
 * Created By: Tim Falken
 * Used to store any serializeable class as JSON files, as well as load and parse JSON files back to objects of specified classes.
 * */

public static class DataSaver 
{
	public static string basePath
	{
		get
		{
			#if UNITY_EDITOR
			return "Assets/StreamingAssets/SaveData";
			#else
			return Application.streamingAssetsPath + "/SaveData";
			#endif
		}
	}

	private static void CreateDirectoryIfNeeded(string _folderName = "")
	{
		if (!System.IO.Directory.Exists (basePath))
		{
			System.IO.Directory.CreateDirectory (basePath);
		}

		if (_folderName != "")
		{
			_folderName += "/";

			if (!System.IO.Directory.Exists (basePath + _folderName))
			{
				System.IO.Directory.CreateDirectory (basePath + _folderName);
			}
		}
	}

	private static string GetDataPathForFile(string _fileName, string _folderName = "")
	{
		if (_folderName != "")
		{
			_folderName += "/";
		}

		return System.IO.Path.Combine (basePath + _folderName, _fileName);
	}

	/**
	 * Check if a given file exists. Optionally include a subfolder.
	 * */
	public static bool FileExists(string _fileName, string _folderName = "")
	{	
		CreateDirectoryIfNeeded (_folderName);

		string path = GetDataPathForFile (_fileName, _folderName);

		return System.IO.File.Exists (path);
	}

	/**
	 * Check if a given file exists. Optionally include a subfolder.
	 * */
	public static string[] GetSaveNamesForFolder(string _folderName = "")
	{	
		CreateDirectoryIfNeeded (_folderName);

		string[] names = System.IO.Directory.GetFiles (basePath + _folderName);

		for (int i = 0; i < names.Length; i++)
		{
			string[] split = System.Text.RegularExpressions.Regex.Split (names [i], "\\\\");

			names [i] = split [split.Length - 1];
		}

		return names;
	}

	/**
	 * Parse a file to an object of a given type. Optionally include a subfolder.
	 * Throws an exception if the file was not found; Use DataSaver.FileExists to check beforehand.
	 * */
	public static T ParseFileToObject<T>(string _fileName, string _folderName = "")
	{
		CreateDirectoryIfNeeded (_folderName);

		string path = GetDataPathForFile (_fileName,_folderName);

		return ParseFileToObjectDirect<T> (path);
	}

	/**
	 * Parse a file to an object of a given type.
	 * Throws an exception if the file was not found.
	 * */
	public static T ParseFileToObjectDirect<T>(string path)
	{
		if (System.IO.File.Exists (path))
		{
			string jsonData = System.IO.File.ReadAllText (path);

			T track = JsonUtility.FromJson<T> (jsonData);

			return track;
		}

		throw new Exception("File does not exist (" + path + ")");
	}

	/**
	 * Save the object as a file in JSON format. Optionally include a subfolder to store in.
	 * */
	public static void SaveObjectAsFile<T> (this T _toSave, string _fileName, string _folderName = "")
	{
		CreateDirectoryIfNeeded (_folderName);

		string path = GetDataPathForFile (_fileName, _folderName);

		string json = JsonUtility.ToJson (_toSave);

		System.IO.File.WriteAllText (path, json);
	}

    public static void DeleteFile (string _fileName, string _folderName = "")
    {
        string path = GetDataPathForFile(_fileName, _folderName);

        System.IO.File.Delete(path);
    }
}