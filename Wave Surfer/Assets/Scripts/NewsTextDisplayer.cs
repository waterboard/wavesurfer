using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * NewsTextDisplayer
 * Created by: Tim Falken
 **/

public class NewsTextDisplayer : MonoBehaviour 
{
	[SerializeField] private Text textBar;

	[SerializeField] private float timePerCharacter = 0.1f;

	[SerializeField] private GameObject newsDisplay, currencyDisplay;

	private float timer = 0;

	private bool newsJingleNeedsPlaying = false;
	[SerializeField] private AudioClip newsJingle;

	private NewsMessages messages = new NewsMessages();
	public NewsMessages Messages
	{
		get
		{
			return messages;
		}
	}

	private int textProgress;
	private string message;

	public Queue<string> newsMessages = new Queue<string> ();

	public void ShowMessage(string message)
	{
		message += "                                                                                            "; //reasons.
		this.message = message;
		textProgress = 0;
	}

	public void QueueMessage(string message)
	{
		newsMessages.Enqueue (message);
	}

	void Start()
	{
		if (DataSaver.FileExists ("News"))
		{
			messages = DataSaver.ParseFileToObject<NewsMessages> ("News");
		}
	}

	public void OnDestroy()
	{
		messages.SaveObjectAsFile ("News");
	}

	void Update()
	{
		if (message != "" && message != null)
		{
			if (timer <= 0)
			{
				textProgress++;
				timer = timePerCharacter;

				if (textProgress >= message.Length)
				{
					message = "";
					return;
				}

				textBar.text = message.Substring (0, textProgress);

				if (textBar.text.Length > 0 && textProgress % (int)UnityEngine.Random.Range((int)1, (int)2) == 0)
				{
					string newChar = textBar.text.Substring (textBar.text.Length - 1);

					if (newChar != " " && newChar != "-" && newChar != ".")
					{
						SingletonManager.NewsController.instance.Speak ();
					}
				}
			}

			timer -= Time.deltaTime;
		} 
		else
		{
			if (newsMessages.Count > 0)
			{
				ShowMessage (newsMessages.Dequeue ());
			} 
			else
			{
				textBar.text = "";
			}
		}

		bool messagePlaying = (message != "" && message != null);

		currencyDisplay.SetActive (!messagePlaying);
		newsDisplay.SetActive (messagePlaying);

		if (messagePlaying && newsJingleNeedsPlaying)
		{
			newsJingleNeedsPlaying = false;
			SingletonManager.SFXPlayer.instance.PlaySound (newsJingle);
		}

		if (!messagePlaying)
		{
			newsJingleNeedsPlaying = true;
		}
	}

	[System.Serializable]
	public class NewsMessages
	{
		//main story (IN ORDER!)
		public NewsMessage BitCoinCreation = new NewsMessage("A new form of currency, Bitcoin, has been invented. Value of coins is low, and the currency is not expected to survive for long.");
		public NewsMessage BitCoinAdoption = new NewsMessage("Bitcoin is struggling, but early adopters insist it will take off \"very soon\".");
		public NewsMessage BitCoinStart = new NewsMessage("Despite slow start, Bitcoin value seems to slowly be rising.");
		public NewsMessage BitcoinPricesRising = new NewsMessage ("BREAKING: Bitcoin value rising quickly!");
		public NewsMessage BitcoinPopularityStarted = new NewsMessage ("BREAKING: Bitcoin becoming popular with new generation. Many stores now accept payments made with Bitcoin.");
		public NewsMessage BitcoinDestroysBanks = new NewsMessage ("BREAKING: Popularity of Bitcoin causes many businesses to abandon banks. Several small banks have closed down, leaving many rushing to withdraw their funds while they still can.");
		public NewsMessage BitcoinNearingPeak = new NewsMessage ("BREAKING: First-world countries widely adopting Bitcoin as national currency.");
		public NewsMessage BitcoinMakesPhysicalMoneyObsolete = new NewsMessage ("BREAKING: Steel and metal prices dropping as physical currency is recycled. Construction work booming.");
		public NewsMessage BitcoinPeakReached = new NewsMessage ("BREAKING: Bitcoins adopted worldwide as global currency. International trade booming, despite warnings by economic conspiracy theorists.");
		public NewsMessage BitcoinPricesDropping = new NewsMessage ("BREAKING: Bitcoin value starts dropping. Will it recover?");
		public NewsMessage BitcoinPricesCrashing = new NewsMessage ("BREAKING: Bitcoin value seems to be crashing severely due to 90% of all bitcoins being owned by a single individual.");
		public NewsMessage BitCoinDestroyedEconomy = new NewsMessage ("BREAKING: World-wide currency value approaces zero. Economy almost non-existent. Many are refusing work, governments are disbanding around the globe.");
		public NewsMessage BitCoinDestroysMedicine = new NewsMessage ("BREAKING: Current economic state renders pharmaceutical companies unsustainable. As a result, pandemic grips the world now that it goes without medicine");
		public NewsMessage BitCoinDestroyedWorld = new NewsMessage ("BREAKING: As world population approaces zero, food sources are scarce. Cases of cannibalism reported.");

		//interesting events (unrelated to progress, but fun to show)
		public NewsMessage TenSattelitesLaunched = new NewsMessage ("BREAKING: A surge in privately-owned sattelites have caused component prices to climb steeply. Smaller space agencies are declaring bankruptcy.");
		public NewsMessage ArtificialMoonBuilt = new NewsMessage ("BREAKING: A new moon has been discovered orbiting Earth. NASA insists it honestly wasn't there before.");
		public NewsMessage TenArtificialPlanetsBuilt = new NewsMessage ("BREAKING: Designer planets popularity rising more than expected.");
		public NewsMessage HundredOldPCsBought = new NewsMessage ("BREAKING: Second-Hand PC's often too expensive for private use.");
		public NewsMessage TenGamingPCsBought = new NewsMessage ("BREAKING: Expensive Gaming Rigs continue to sell well despite increasingly steep prices.");
		public NewsMessage DimensionalRiftOpened = new NewsMessage ("BREAKING: Alien Conquerers of Peace welcomed by tolerant college students.");
		public NewsMessage DysonSphereConstructed = new NewsMessage ("BREAKING: Earth plummets into ice age as the sun is blocked by an artificial structure.");
		public NewsMessage QuantumComputerInvented = new NewsMessage ("BREAKING: Bitcoin enthousiast perfects Quantum Computing. Uses invention to mine Bitcoin faster.");
		public NewsMessage FiveServerGridsBought = new NewsMessage ("BREAKING: Privately-owned server grids discovered. Purpose unknown.");
		public NewsMessage TenServerGridsBought = new NewsMessage ("BREAKING: Home owners left in the dark as privately-owned server grids consume nearly all electricity produced.");
		public NewsMessage ThreeSupercomputersBought = new NewsMessage ("BREAKING: Military-grade supercomputers reportedly sold to unknown sources. Investigation ongoing.");

		public NewsMessage HundredRamSticks = new NewsMessage ("BREAKING: Computer RAM prices through the roof. Companies worldwide scrambling to actually optimize their products.");
		public NewsMessage FiftyGPUsBought = new NewsMessage ("BREAKING: Many graphics cards too expensive for non-enthousiasts. Text-based games are making comebacks.");
		public NewsMessage FiftyServerGridsBought = new NewsMessage ("BREAKING: Popular search engine declares emergency as it can no longer sustain server grids due to massive price inflation.");
		public NewsMessage BillGatesBeaten = new NewsMessage ("BREAKING: Bill Gates no longer richest man on earth.");
		public NewsMessage TwentyServerGridsBuilt = new NewsMessage ("BREAKING: Small village gone homeless: \"Turns out some person owns the entire area now, and he's tearing it down for Bitcoin.\".");
		public NewsMessage CarsTooExpensive = new NewsMessage ("BREAKING: CO2 levels dropping harshly as cars become an unaffordable luxury.");
		public NewsMessage TwoHundredPCsBought = new NewsMessage ("BREAKING: US Government sells legacy hardware to Bitcoin enthousiast, pays off national debt entirely.");
	}

	[System.Serializable]
	public class NewsMessage
	{
		public NewsMessage()
		{
			HasPlayed = false;
		}

		public NewsMessage(string message)
		{
			HasPlayed = false;
			Message = message;
		}

		public void QueueIfNotPlayed()
		{
			if (!HasPlayed)
			{
				HasPlayed = true;

				Debug.Log ("QUEUED: " + Message);

				SingletonManager.NewsTextDisplayer.instance.QueueMessage (Message);
			}
		}

		public bool HasPlayed = false;
		public string Message;
	}
}
