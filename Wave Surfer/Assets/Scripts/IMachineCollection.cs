﻿/**
 * IMachineCollection
 * Created by: Tim Falken
 **/

public interface IMachineCollection  
{
	void Save();
	void Load();
	void AttemptBuy();
	double GetBuildCost();
}
