﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * RamStickContainer
 * Created by: Tim Falken
 **/

public class RamStickContainer : MonoBehaviour, IMachineCollection
{
	private static List<RamStickContainer> all = new List<RamStickContainer> ();

	public static double TotalIncomePerSecond
	{
		get
		{
			double result = 0;

			for (int i = 0; i < all.Count; i++)
			{
				result += all [i].generator.BTCPerSecond;
			}

			return result;
		}
	}

	private bool unlocked;

	private string nameFieldText;
	[SerializeField] private Text nameText;

	[SerializeField] private AudioClip buildClip;

	[SerializeField] private Text gainPerTickText;

	[SerializeField] private MachineTypes name;
	public MachineSaveData Data = new MachineSaveData ();
	private BTCGenerator generator;

	[SerializeField] private double baseBTCGeneration = CurrencyManager.OneUSDInBTC;
	[SerializeField] private float baseInterval = 15;

	[SerializeField] private double baseCost = 0.00001;
	const double PriceMultiplier = 1.05;

	[SerializeField] private Image imageBack, imageFront;

	[SerializeField] private Sprite frontSprite, lockSprite;

	[SerializeField] private Scrollbar progressBar;
	[SerializeField] private Text buyCostText;
	[SerializeField] private Text amountText;

	void Start () 
	{
		nameFieldText = nameText.text;
		generator = new BTCGenerator (baseBTCGeneration, baseInterval);

		Load ();	

		buyCostText.text = GetBuildCost ().Round(6).ToString("##.######");

		ApplyToGenerator ();

		all.Add (this);
	}

	void BounceIcon()
	{
		Tween bounceIn = new Tween (Easing.EaseOutQuad);

		bounceIn.onChange = t =>
		{
			imageFront.transform.localScale = Vector3.one * t;
		};

		bounceIn.onComplete = () =>
		{
			Tween bounceOut = new Tween (Easing.EaseInElastic);

			bounceOut.onChange = t =>
			{
				imageFront.transform.localScale = Vector3.one * t;
			};

			bounceOut.Start(1.4f, 1, 0.5f);
		};

		bounceIn.Start (1, 1.4f, 0.1f);
	}

	void OnDestroy()
	{
		Save ();
	}

	private void ApplyToGenerator()
	{
		generator.Amount = Data.AmountOwned;
	}
	
	void Update () 
	{
		unlocked = Data.AmountOwned > 0 || CurrencyManager.BTC >= GetBuildCost();

		nameText.text = unlocked ? nameFieldText : "???";

		imageFront.overrideSprite = unlocked ? null : lockSprite;

		gainPerTickText.text = unlocked? (Data.AmountOwned > 0? ("+" + generator.TrueOutputPerTick.Round (5).ToString ("##.######")) : ("(" + generator.BTCPerTick.Round(5).ToString("##.######") + " BTC/s)")) : "?";

		buyCostText.text = GetBuildCost ().Round(6).ToString("##.################");

		buyCostText.color = CurrencyManager.CanSpend (GetBuildCost ()) ? Color.white : Color.red;

		amountText.text = Data.AmountOwned.ToString ();

		if (Data.AmountOwned > 0)
		{
			generator.Update ();

			ApplyToGenerator ();

			progressBar.size = generator.TimerProgress;
		} 
		else
		{
			progressBar.size = 0;
		}
	}

	#region IMachineCollection implementation

	public void Save ()
	{
		Data.SaveObjectAsFile (name.ToString());
	}

	public void Load ()
	{
		if (DataSaver.FileExists (name.ToString()))
		{
			Data = DataSaver.ParseFileToObject<RamStickContainer.MachineSaveData> (name.ToString());
		}
	}

	public void AttemptBuy ()
	{
		if (CurrencyManager.CanSpend (GetBuildCost ()))
		{
			CurrencyManager.ReduceBTC (GetBuildCost ());
			Data.AmountOwned++;

			BounceIcon ();

			SingletonManager.SFXPlayer.instance.PlaySound (buildClip);

			CheckNewsToSend ();
		}
	}

	private void CheckNewsToSend()
	{
		Debug.Log("Total income per second is now: " + TotalIncomePerSecond.ToString("##.##########"));

		if (TotalIncomePerSecond >= 0.0001)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.01;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitCoinCreation.QueueIfNotPlayed ();

			SingletonManager.MusicManager.instance.ChangeTrack (MusicManager.MusicTypes.Beginning);
		}

		if (TotalIncomePerSecond >= 0.001)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.1;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitCoinAdoption.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 0.01)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.3;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitCoinStart.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 0.05)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.5;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinPricesRising.QueueIfNotPlayed ();

			SingletonManager.MusicManager.instance.ChangeTrack (MusicManager.MusicTypes.GettingRich);
		}

		if (TotalIncomePerSecond >= 0.5)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 1;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinPopularityStarted.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 1)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 1.2;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinDestroysBanks.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 3)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 1.5;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinNearingPeak.QueueIfNotPlayed ();
			SingletonManager.MusicManager.instance.ChangeTrack (MusicManager.MusicTypes.Peaking);
		}

		if (TotalIncomePerSecond >= 9)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 2;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinMakesPhysicalMoneyObsolete.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 54)
		{
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinPeakReached.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 200)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.9;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinPricesDropping.QueueIfNotPlayed ();
			SingletonManager.MusicManager.instance.ChangeTrack (MusicManager.MusicTypes.Dropping);
		}

		if (TotalIncomePerSecond >= 400)
		{
			SingletonManager.NewsTextDisplayer.instance.Messages.CarsTooExpensive.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 800)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.1;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitcoinPricesCrashing.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 2900)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.001;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitCoinDestroyedEconomy.QueueIfNotPlayed ();

			SingletonManager.MusicManager.instance.ChangeTrack (MusicManager.MusicTypes.Crashed);
		}

		if (TotalIncomePerSecond >= 10000)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0.00001;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitCoinDestroysMedicine.QueueIfNotPlayed ();
		}

		if (TotalIncomePerSecond >= 30000)
		{
			ConversionDisplayer.ValueMultiplier.Multiplier = 0;
			SingletonManager.NewsTextDisplayer.instance.Messages.BitCoinDestroyedWorld.QueueIfNotPlayed ();
		}

		switch (name)
		{
		case MachineTypes.RAM:
			if(Data.AmountOwned >= 100)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.HundredRamSticks.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.GPU:
			if(Data.AmountOwned >= 50)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.FiftyGPUsBought.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.OldPC:
			if (Data.AmountOwned >= 100)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.HundredOldPCsBought.QueueIfNotPlayed ();
			}

			if (Data.AmountOwned >= 200)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.TwoHundredPCsBought.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.ServerGrid:
			if(Data.AmountOwned >= 10)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.TenServerGridsBought.QueueIfNotPlayed ();
			}

			if(Data.AmountOwned >= 50)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.FiftyServerGridsBought.QueueIfNotPlayed ();
			}

			if (Data.AmountOwned > 5)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.FiveServerGridsBought.QueueIfNotPlayed ();
			}

			if (Data.AmountOwned >= 20)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.TwentyServerGridsBuilt.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.Sattelite:
			if (Data.AmountOwned >= 10)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.TenSattelitesLaunched.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.ArtificialMoon:
			SingletonManager.NewsTextDisplayer.instance.Messages.ArtificialMoonBuilt.QueueIfNotPlayed ();
			break;
		case MachineTypes.ArtificialPlanet:
			if (Data.AmountOwned >= 10)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.TenArtificialPlanetsBuilt.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.GamingRig:
			if (Data.AmountOwned >= 10)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.TenGamingPCsBought.QueueIfNotPlayed ();
			}
			break;
		case MachineTypes.DimensionalRift:
			SingletonManager.NewsTextDisplayer.instance.Messages.DimensionalRiftOpened.QueueIfNotPlayed ();
			break;
		case MachineTypes.DysonSphere:
			SingletonManager.NewsTextDisplayer.instance.Messages.DysonSphereConstructed.QueueIfNotPlayed ();
			break;
		case MachineTypes.QuantumPC:
			SingletonManager.NewsTextDisplayer.instance.Messages.QuantumComputerInvented.QueueIfNotPlayed ();
			break;
		case MachineTypes.SuperComputer:
			if (Data.AmountOwned >= 3)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.ThreeSupercomputersBought.QueueIfNotPlayed ();
			}
			break;
		}
	}

	public double GetBuildCost ()
	{
		return (Data.AmountOwned == 0? 
			baseCost : 
			Data.AmountOwned * baseCost * PriceMultiplier
		).Round(5);
	}

	#endregion

	[System.Serializable]
	public class MachineSaveData
	{
		public int AmountOwned = 0;
	}

	public enum MachineTypes
	{
		RAM,
		GPU,
		OldPC,
		GamingRig,
		ServerPC,
		SuperComputer,
		ServerFarm,
		ServerGrid,
		QuantumPC,
		Sattelite,
		SpaceStation,
		ArtificialMoon,
		ArtificialPlanet,
		DysonSphere,
		DimensionalRift
	}
}
