﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * SingletonManager
 * Created by: Tim Falken
 * Use to find instances in the scene without having to map them yourself.
 * Cannot be used for object types of which there are more than one in a scene.
 **/

public class SingletonManager 
{
	public static AutoFind<NewsTextDisplayer> NewsTextDisplayer = new AutoFind<NewsTextDisplayer> ();
	public static AutoFind<MusicManager> MusicManager = new AutoFind<MusicManager> ();
	public static AutoFind<SFXPlayer> SFXPlayer = new AutoFind<SFXPlayer> ();
	public static AutoFind<NewsController> NewsController = new AutoFind<NewsController> ();

	public class AutoFind<T> where T : UnityEngine.Object
	{
		private T _val;

		public T instance
		{
			get
			{
				if (_val == null)
				{
					_val = GameObject.FindObjectOfType<T> ();
				}

				if (_val == null)
				{
					Debug.LogWarning ("Object of type <" + typeof(T).ToString () + "> requested but not found in scene!");
				}

				return _val;
			}
		}

		// Convenience function
		public static implicit operator T(AutoFind<T> val)
		{
			return val.instance;
		}
	}
}