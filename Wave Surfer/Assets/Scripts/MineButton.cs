﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * MineButton
 * Created by: Tim Falken
 **/

public class MineButton : MonoBehaviour 
{
	[SerializeField] private float rotateCockPoint;
	[SerializeField] private float rotateHitPoint;
	[SerializeField] private Image pickImage;

	[SerializeField] private AudioClip[] mineSounds;

	public void Mine()
	{
		SingletonManager.MusicManager.instance.ChangeTrack (MusicManager.MusicTypes.Beginning);

		CurrencyManager.AddBTC (CurrencyManager.OneUSDInBTC);

		Tween twistIn = new Tween (Easing.EaseInQuad);

		twistIn.onChange = t =>
		{
			pickImage.transform.rotation = Quaternion.Euler(0, 0, t);
		};

		twistIn.onComplete = () =>
		{
			Tween twistHit = new Tween (Easing.EaseInQuad);

			twistHit.onChange = t =>
			{
				pickImage.transform.rotation = Quaternion.Euler(0, 0, t);
			};

			twistHit.onComplete = () =>
			{
				Tween twistBack = new Tween (Easing.EaseOutBack);

				twistBack.onChange = t =>
				{
					pickImage.transform.rotation = Quaternion.Euler(0, 0, t);
				};

				twistBack.Start(rotateHitPoint, 0, 0.2f);

				SingletonManager.SFXPlayer.instance.PlaySound(mineSounds[UnityEngine.Random.Range(0, mineSounds.Length)]);
			};

			twistHit.Start(rotateCockPoint, rotateHitPoint, 0.2f);
		};

		twistIn.Start (0, rotateCockPoint, 0.3f);
	}
}
