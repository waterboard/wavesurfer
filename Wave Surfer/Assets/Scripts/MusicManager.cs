﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * MusicManager
 * Created by: Tim Falken
 **/

public class MusicManager : MonoBehaviour 
{
	private MusicData data = new MusicData();
	private AudioSource source;

	private float musicVolume = 0.3f;

	[SerializeField] private AudioClip beginning, gettingRich, peaking, dropping, crashed;

	private Tween fadeTween = new Tween();

	void Start () 
	{
		source = GetComponent<AudioSource> ();

		if (DataSaver.FileExists ("Music"))
		{
			data = DataSaver.ParseFileToObject<MusicData> ("Music");
		}

		fadeTween.onChange = t =>
		{
			source.volume = t;
		};

		ChangeTrack (data.CurrentTrack, true);
	}
	
	void OnDestroy () 
	{
		data.SaveObjectAsFile ("Music");
	}

	public void ChangeTrack(MusicTypes newType, bool force = false)
	{
		Debug.Log(((int)data.CurrentTrack < (int)newType || force)? "Need switch!" : "Switch asked, not needed");

		if ((int)data.CurrentTrack < (int)newType || force)
		{
			data.CurrentTrack = newType;

			fadeTween.onComplete = () =>
			{
				switch (newType)
				{
				case MusicTypes.Beginning:
					source.clip = beginning;
					break;
				case MusicTypes.GettingRich:
					source.clip = gettingRich;
					break;
				case MusicTypes.Peaking:
					source.clip = peaking;
					break;
				case MusicTypes.Dropping:
					source.clip = dropping;
					break;
				case MusicTypes.Crashed:
					source.clip = crashed;
					break;
				}

				source.Play ();

				fadeTween.Start (0, musicVolume, 1);

				fadeTween.onComplete = () =>
				{
					Debug.Log ("Music switched");
				};
			};

			fadeTween.Start (musicVolume, 0, 1);
		}
	}

	[System.Serializable]
	public class MusicData
	{
		public MusicTypes CurrentTrack = MusicTypes.Nothing;
	}

	public enum MusicTypes
	{
		Nothing,
		Beginning,
		GettingRich,
		Peaking,
		Dropping,
		Crashed
	}
}
