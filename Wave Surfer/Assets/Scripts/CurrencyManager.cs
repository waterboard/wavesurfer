﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * CurrencyManager
 * Created by: Tim Falken
 **/

public class CurrencyManager 
{
	public const double OneUSDInBTC = 0.0000895297;

	private static double _btc = OneUSDInBTC;

	public static double BTC
	{
		get
		{
			return _btc;
		}

		set
		{
			_btc = value;

			if (CurrencyManager.USD > 93000000000)
			{
				SingletonManager.NewsTextDisplayer.instance.Messages.BillGatesBeaten.QueueIfNotPlayed ();
			}
		}
	}

	public static double USD
	{
		get
		{
			return BTC * 11169.48;
		}
	}

	public static double EUR
	{
		get
		{
			return USD * 0.805152979;
		}
	}

	public static double GBP
	{
		get
		{
			return USD * 0.705701361;
		}
	}

	public static double JPY
	{
		get
		{
			return USD * 108.695652;
		}
	}

	public static double RUB
	{
		get
		{
			return USD * 56.2429696;
		}
	}

	public static void AddBTC(double value)
	{
		BTC += value;
	}

	public static void ReduceBTC(double value)
	{
		BTC -= value;
	}

	public static bool CanSpend(double value)
	{
		return BTC >= value;
	}

	public static void Save()
	{
		BTCSaveData btcData = new BTCSaveData ();

		btcData.BTC = BTC;

		btcData.SaveObjectAsFile ("btc");
	}

	public static void Load()
	{
		if (DataSaver.FileExists ("btc"))
		{
			BTCSaveData saveData = DataSaver.ParseFileToObject<BTCSaveData> ("btc");
			BTC = saveData.BTC;
		}
	}

	[System.Serializable]
	public class BTCSaveData
	{
		public double BTC;
	}
}
