﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

/**
 * ConversionDisplayer
 * Created by: Tim Falken
 **/

public class ConversionDisplayer : MonoBehaviour 
{
	[SerializeField] private Text usd, eur, gbp, jpy, rub;

	public static MultiplierStorage ValueMultiplier = new MultiplierStorage();

	private double multiplierTarget = 0;
	private double multiplierDisplayed = 0;

	Tween multTween = new Tween();

	void Update () 
	{
		if (multiplierTarget != ValueMultiplier.Multiplier)
		{
			multiplierTarget = ValueMultiplier.Multiplier;

			multTween.Abort ();
			multTween.Start ((float)multiplierDisplayed, (float)multiplierTarget, 1);
		}

		usd.text = FormatCurrency ((CurrencyManager.USD * multiplierDisplayed).Round (5), "$");
		eur.text = FormatCurrency ((CurrencyManager.EUR * multiplierDisplayed).Round(5), "€");
		gbp.text = FormatCurrency ((CurrencyManager.GBP * multiplierDisplayed).Round(5), "£");
		jpy.text = FormatCurrency ((CurrencyManager.JPY * multiplierDisplayed).Round(5), "¥");
		rub.text = FormatCurrency ((CurrencyManager.RUB * multiplierDisplayed).Round(5), "₽");
	}

	public void Start()
	{
		if (DataSaver.FileExists ("GlobalValue"))
		{
			ValueMultiplier = DataSaver.ParseFileToObject<MultiplierStorage> ("GlobalValue");
		}

		multTween.onChange = t =>
		{
			multiplierDisplayed = (double)t;
		};
	}

	public void OnDestroy()
	{
		ValueMultiplier.SaveObjectAsFile ("GlobalValue");
	}

	private string FormatCurrency(double amount, string currencySymbol)
	{
		NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
		nfi = (NumberFormatInfo) nfi.Clone();

		nfi.CurrencySymbol = currencySymbol;
		return string.Format(nfi, "{0:c}", amount);
	}

	[System.Serializable]
	public class MultiplierStorage
	{
		public double Multiplier = 0.005;
	}
}
