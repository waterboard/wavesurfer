﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * CurrencyDisplayer
 * Created by: Tim Falken
 **/

public class CurrencyDisplayer : MonoBehaviour 
{
	[SerializeField] private Text textField;

	void Start()
	{
		CurrencyManager.Load ();
	}

	void OnDestroy()
	{
		CurrencyManager.Save ();
	}

	void Update () 
	{
		textField.text = CurrencyManager.BTC.Round(5).ToString ("Ƀ##.######");
	}
}

public static class RoundHelper
{
	public static double Round(this double val, int maxDecimalsOnSingleDigits)
	{
		int digitsBeforeDecimal = Math.Round (val).ToString ().Length;
		return Math.Round (val, Math.Max (0, maxDecimalsOnSingleDigits - digitsBeforeDecimal));
	}
}