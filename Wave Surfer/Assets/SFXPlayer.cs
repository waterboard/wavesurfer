﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * SFXPlayer
 * Created by: Tim Falken
 **/

public class SFXPlayer : MonoBehaviour 
{
	private AudioSource source;

	void Start () 
	{
		source = GetComponent<AudioSource> ();
	}
	
	public void PlaySound(AudioClip clip, float volume = 1)
	{
		source.PlayOneShot (clip, volume);
	}
}
