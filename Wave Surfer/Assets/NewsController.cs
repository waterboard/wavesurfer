﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * NewsController
 * Created by: Tim Falken
 **/

public class NewsController : MonoBehaviour 
{
	[SerializeField] private AudioClip[] voices;

	[SerializeField] private Image head, paper;
	[SerializeField] private RectTransform headAnchor;

	void Update () 
	{
		head.rectTransform.localPosition = Vector3.Lerp (head.transform.localPosition, headAnchor.localPosition, Time.deltaTime * 10);
	}

	public void Speak()
	{
		head.transform.localPosition += UnityEngine.Random.onUnitSphere * 4f;

		SingletonManager.SFXPlayer.instance.PlaySound(voices[UnityEngine.Random.Range(0, voices.Length)], 0.4f);
	}
}
