﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

public class Compressor {
	[PostProcessBuildAttribute(1)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
		Debug.Log( pathToBuiltProject );
		System.Diagnostics.Process.Start ("compress.bat");
	}
}